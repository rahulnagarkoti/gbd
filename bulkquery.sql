BULK INSERT abc
FROM 'C:\Users\Nagarkoti\Python\file.csv'
WITH
(
    FIRSTROW = 2,
    FIELDTERMINATOR = ',',  --CSV field delimiter
    ROWTERMINATOR = '0x0a',   --Use to shift the control to next row--Hexa
    TABLOCK
)